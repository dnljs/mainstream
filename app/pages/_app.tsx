import { AppProps, ErrorComponent } from "blitz"
import { ErrorBoundary, FallbackProps } from "react-error-boundary"
import { queryCache } from "react-query"
import Container from "@material-ui/core/Container"

import LoginForm from "app/auth/components/LoginForm"
import { create } from "jss"
import rtl from "jss-rtl"
import CssBaseline from "@material-ui/core/CssBaseline"
import { createMuiTheme, jssPreset, StylesProvider, ThemeProvider } from "@material-ui/core/styles"

import Appbar from "app/components/Appbar"

import React from "react"

const jss = create({ plugins: [...jssPreset().plugins, rtl()] })

//You can customize this as you want and even move it out to a separate file
const theme = createMuiTheme({
  palette: {
    type: "light",
    primary: {
      main: "#000",
    },
  },
  direction: "rtl",
})

export default function App({ Component, pageProps }: AppProps) {
  const getLayout = Component.getLayout || ((page) => page)

  return (
    <ErrorBoundary
      FallbackComponent={RootErrorFallback}
      onReset={() => {
        // This ensures the Blitz useQuery hooks will automatically refetch
        // data any time you reset the error boundary
        queryCache.resetErrorBoundaries()
      }}
    >
      {getLayout(
        <ThemeProvider theme={theme}>
          <StylesProvider jss={jss}>
            <div dir="rtl">
              <CssBaseline />
              <Container style={{ margin: "0", padding: "0" }}>
                <Appbar />
                <Component {...pageProps} />
              </Container>
            </div>
          </StylesProvider>
        </ThemeProvider>
      )}
    </ErrorBoundary>
  )
}

function RootErrorFallback({ error, resetErrorBoundary }: FallbackProps) {
  if (error?.name === "AuthenticationError") {
    return <LoginForm onSuccess={resetErrorBoundary} />
  } else if (error?.name === "AuthorizationError") {
    return (
      <ErrorComponent
        statusCode={(error as any).statusCode}
        title="Sorry, you are not authorized to access this"
      />
    )
  } else {
    return (
      <ErrorComponent
        statusCode={(error as any)?.statusCode || 400}
        title={error?.message || error?.name}
      />
    )
  }
}
