import { BlitzPage } from "blitz"
import Layout from "app/layouts/Layout"
import { useWindowSize } from "../../utils/hooks/uesWindowSize"
import StepOne from "app/components/gameSteps/StepOne"
import StepTwo from "app/components/gameSteps/StepTwo"
import StepThree from "app/components/gameSteps/StepThree"
import { useEffect, useState } from "react"

const Home: BlitzPage = () => {
  const size: { width: any; height: any } = useWindowSize()
  const [step, setStep] = useState(1)
  const [lead, setLead] = useState({})
  const stepUp = () => setStep((state) => ++state)
  const userForm = (data) => setLead(data)

  function Stepper({ children }) {
    return <>{children}</>
  }

  useEffect(() => {
    console.log(lead)
  }, [lead])
  const Step = (step) => {
    switch (step) {
      case 1:
        return (
          <Stepper>
            <StepOne stepUp={stepUp} />
          </Stepper>
        )
      case 2:
        return (
          <Stepper>
            <StepTwo userForm={userForm} stepUp={stepUp} />
          </Stepper>
        )
      case 3:
        return (
          <Stepper>
            <StepThree lead={lead} />
          </Stepper>
        )

      default:
        return "something went wrong"
    }
  }

  return (
    <div className="container">
      {size.width > 780 && <div>game component</div>}
      {size.width < 780 && <>{Step(step)}</>}
      <style jsx global>{`
        html,
        body {
          padding: 0;
          margin: 0;
          font-family: "Libre Franklin", -apple-system, BlinkMacSystemFont, Segoe UI, Roboto, Oxygen,
            Ubuntu, Cantarell, Fira Sans, Droid Sans, Helvetica Neue, sans-serif;
        }
        * {
          -webkit-font-smoothing: antialiased;
          -moz-osx-font-smoothing: grayscale;
          box-sizing: border-box;
        }
        .container {
          min-height: 100vh;
          display: flex;
          flex-direction: column;
          justify-content: center;
          align-items: center;
        }
        main {
          padding: 5rem 0;
          flex: 1;
          display: flex;
          flex-direction: column;
          justify-content: center;
          align-items: center;
        }

        main p {
          font-size: 1.2rem;
        }

        p {
          text-align: center;
        }

        footer {
          width: 100%;
          height: 60px;
          border-top: 1px solid #eaeaea;
          display: flex;
          justify-content: center;
          align-items: center;
          background-color: #45009d;
        }

        footer a {
          display: flex;
          justify-content: center;
          align-items: center;
        }

        footer a {
          color: #f4f4f4;
          text-decoration: none;
        }

        .logo {
          margin-bottom: 2rem;
        }

        .logo img {
          width: 300px;
        }

        .buttons {
          display: grid;
          grid-auto-flow: column;
          grid-gap: 0.5rem;
        }
        .button {
          font-size: 1rem;
          background-color: #6700eb;
          padding: 1rem 2rem;
          color: #f4f4f4;
          text-align: center;
        }

        .button.small {
          padding: 0.5rem 1rem;
        }

        .button:hover {
          background-color: #45009d;
        }

        .button-outline {
          border: 2px solid #6700eb;
          padding: 1rem 2rem;
          color: #6700eb;
          text-align: center;
        }

        .button-outline:hover {
          border-color: #45009d;
          color: #45009d;
        }

        pre {
          background: #fafafa;
          border-radius: 5px;
          padding: 0.75rem;
        }
        code {
          font-size: 0.9rem;
          font-family: Menlo, Monaco, Lucida Console, Liberation Mono, DejaVu Sans Mono,
            Bitstream Vera Sans Mono, Courier New, monospace;
        }

        .grid {
          display: flex;
          align-items: center;
          justify-content: center;
          flex-wrap: wrap;

          max-width: 800px;
          margin-top: 3rem;
        }

        @media (max-width: 600px) {
          .grid {
            width: 100%;
            flex-direction: column;
          }
        }

        #root > div {
          width: 100%;
          height: 100%;
          display: flex;
          align-items: center;
          justify-content: center;
        }

        .c {
          position: absolute;
          max-width: 500px;
          max-height: 500px;
          width: 50ch;
          height: 50ch;
          cursor: pointer;
          will-change: transform, opacity;
        }

        .front,
        .back {
          background-size: cover;
        }

        .back {
          background-image: url(https://images.unsplash.com/photo-1544511916-0148ccdeb877?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&w=1901&q=80i&auto=format&fit=crop);
        }

        .front {
          background-image: url(https://images.unsplash.com/photo-1540206395-68808572332f?ixlib=rb-1.2.1&w=1181&q=80&auto=format&fit=crop);
        }
      `}</style>
    </div>
  )
}

Home.getLayout = (page) => <Layout title="Home">{page}</Layout>

export default Home
