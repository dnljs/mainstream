import React from "react"
import ScratchCard from "react-scratchcard"
import createLead from "app/leads/mutations/createLead"
import updatePrize from "app/prizes/mutations/updatePrize"
import convert from "xml-js"
import axios from "axios"

function ScratchTicket({ prize, lead }) {
  console.log(prize?.temp?.tossed, "prize")
  const tostted = prize?.temp?.tossed
  const settings = {
    width: 320,
    height: 441,
    image: "images/scratch.png",
    finishPercent: 50,
    onComplete: async () => {
      try {
        const newLead = await createLead({
          data: { ...lead, prize: { connect: { id: tostted.id } } },
        })
        const updatedPrize = updatePrize({
          where: {
            id: tostted.id,
          },
          data: { quantity: tostted.quantity - 1 },
        })
        const smsTemplate = `
       <Inforu>
       <User>
       <Username>laba</Username>
       <ApiToken>laba2536</ApiToken>
       </User>
       <Content Type="sms">
       <Message>This is a test SMS Message</Message>
       </Content>
       <Recipients>
       <PhoneNumber>0501111111;0502222222</PhoneNumber>
       </Recipients>
       <Settings>
       <Sender>0502466626</Sender>
       </Settings>
      </Inforu>
        `

        const result1 = convert.xml2json(smsTemplate, { compact: true, spaces: 4 })
        const sent = await axios.post(
          "https://uapi.inforu.co.il/SendMessageXml.ashx", ``
        )

        console.log(newLead, updatedPrize, "new lead", sent)
      } catch (error) {
        console.log(error)
      }
    },
  }
  return (
    <div
      style={{
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        alignContent: "center",
        marginTop: "3.5vh",
      }}
    >
      <ScratchCard {...settings}>
        <img style={{ width: "320px" }} src={prize?.temp?.tossed?.imgUrl} />
      </ScratchCard>
    </div>
  )
}

export default ScratchTicket
