import React from "react"

import Button from "@material-ui/core/Button"
import Grid from "@material-ui/core/Grid"
import { animated, useSpring } from "react-spring"
import { Animated } from "react-animated-css"

function FirstScreen({ stepUp }) {
  const springFade = useSpring({ opacity: 1, from: { opacity: 0 } })
  return (
    <animated.div style={springFade}>
      <div className="container">
        <Grid container>
          <Grid item xs={12}>
            <Animated
              animationInDuration={7500}
              animationInDelay={750}
              animateOnMount={true}
              animationIn="headShake"
              animationOut="headShake"
              animationIterationCount="infinite"
              isVisible={true}
            >
              <img alt="hand-img" className="hand-img" src="images/hand.png" />
            </Animated>
          </Grid>
          <div className="concept">
            <img className="concept-logo" src="images/concept-logo.png" alt="concept-logo" />
            <Button
              onClick={stepUp}
              variant="contained"
              size="large"
              color="primary"
              style={{
                marginTop: "10vh",
                width: "50vw",
                padding: "0.75em",
                fontSize: "1.5rem",
                borderRadius: "18px",
              }}
            >
              {`למשחק>`}
            </Button>
          </div>
        </Grid>

        {/* styles */}

        <style jsx>{`
          .container {
            background: url("images/bg1.png");
            background-size: cover;
            min-height: 95vh;
            padding: 0;
          }
          .concept {
            display: flex;
            flex-direction: column;
            justify: center;
            align-items: center;
            justify-items: center;
            justify-content: center;
            justify-self: center;
            margin: 0 auto;
          }
          .concept-logo {
            width: 65%;
            margin-top: -7vh;
          }
          .hand-img {
            width: 90.5%;
          }
        `}</style>
      </div>
    </animated.div>
  )
}

export default FirstScreen
