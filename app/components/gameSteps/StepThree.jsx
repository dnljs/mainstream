import React, { useEffect, useState } from "react"
import Grid from "@material-ui/core/Grid"
import Typography from "@material-ui/core/Typography"
import ScratchTicket from "app/components/ScratchTicket"
import getRandomPrize from "app/prizes/queries/getRandomPrize"

function StepThree({ lead }) {
  const [tossedPrize, setTossedPrize] = useState({})
  useEffect(async () => {
    let temp = await getRandomPrize()
    setTossedPrize({ temp })
    console.log(temp)
  }, [])
  if (tossedPrize) {
    return (
      <div className="container">
        <Grid container>
          <Grid item xs={12}>
            <div style={{ width: "100%" }}>
              <ScratchTicket lead={lead} prize={tossedPrize} />
            </div>

            <div
              style={{
                textAlign: "center",
                display: "flex",
                justifyContent: "center",
                marginTop: "3vh",
              }}
            >
              <Typography variant="subtitle1">
                <span
                  style={{
                    color: "black",
                    fontWeight: "300",
                    fontFamily: "Rubik",
                  }}
                >
                  יש לגרד את האיזור הכסוף מעלה באמצעות
                  <br />
                  האצבע עד לחשיפה מלאה
                </span>
              </Typography>
            </div>
          </Grid>
        </Grid>
        {/* styles */}
        <style jsx>{`
          .container {
            background: url("images/yellow-bg.png");
            background-size: cover;
            min-height: 100vh;
            width: 100%;
            padding: 0;
          }
          .concept {
            display: flex;
            flex-direction: column;
            justify: center;
            align-items: center;
            justify-items: center;
            justify-content: center;
            justify-self: center;
            margin: 0 auto;
          }
          .concept-logo {
            width: 65%;
            margin-top: -7vh;
          }
          .hand-img {
            width: 90.5%;
            margin-top: 6vh;
          }
        `}</style>
      </div>
    )
  } else {
    return(
      <div>Loading</div>
      );
  }
}

export default StepThree
