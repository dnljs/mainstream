import { SessionContext } from "blitz"
import db, { PrizeDeleteArgs } from "db"

type DeletePrizeInput = {
  where: PrizeDeleteArgs["where"]
}

export default async function deletePrize(
  { where }: DeletePrizeInput,
  ctx: { session?: SessionContext } = {}
) {
  ctx.session!.authorize()

  const prize = await db.prize.delete({ where })

  return prize
}
