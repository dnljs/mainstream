import { SessionContext } from "blitz"
import db, { PrizeCreateArgs } from "db"

type CreatePrizeInput = {
  data: PrizeCreateArgs["data"]
}
export default async function createPrize(
  { data }: CreatePrizeInput,
  ctx: { session?: SessionContext } = {}
) {
  ctx.session!.authorize()

  const prize = await db.prize.create({ data })

  return prize
}
