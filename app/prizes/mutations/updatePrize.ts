import { SessionContext } from "blitz"
import db, { PrizeUpdateArgs } from "db"

type UpdatePrizeInput = {
  where: PrizeUpdateArgs["where"]
  data: PrizeUpdateArgs["data"]
}

export default async function updatePrize({ where, data }: UpdatePrizeInput) {
  const prize = await db.prize.update({ where, data })
  return prize
}
