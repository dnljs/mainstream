import { SessionContext } from "blitz"
import db, { FindManyPrizeArgs } from "db"

type GetPrizesInput = {
  where?: FindManyPrizeArgs["where"]
  orderBy?: FindManyPrizeArgs["orderBy"]
  skip?: FindManyPrizeArgs["skip"]
  take?: FindManyPrizeArgs["take"]
  // Only available if a model relationship exists
  // include?: FindManyPrizeArgs['include']
}

export default async function getRandomPrize() {
  const toss = Math.floor(Math.random() * 10000) + 1
  const prizeing = await db.prize.findMany({
    where: {
      quantity: {
        gt: 0,
      },
    },
  })
  const shaffled = prizeing.filter((item) => {
    if (toss > 9900) {
      return item.chances === 1
    }
    if (toss > 9500) {
      return item.chances === 10
    } else {
      return item.chances === 50
    }
  })
  const tossed = shaffled[Math.floor(Math.random() * shaffled.length)]
  return {
    tossed,
  }
}
