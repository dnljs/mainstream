import { NotFoundError, SessionContext } from "blitz"
import db, { FindOnePrizeArgs } from "db"

type GetPrizeInput = {
  where: FindOnePrizeArgs["where"]
  // Only available if a model relationship exists
  // include?: FindOnePrizeArgs['include']
}

export default async function getPrize(
  { where /* include */ }: GetPrizeInput,
  ctx: { session?: SessionContext } = {}
) {
  ctx.session!.authorize()

  const prize = await db.prize.findOne({ where })

  if (!prize) throw new NotFoundError()

  return prize
}
