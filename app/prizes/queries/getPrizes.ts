import db, { FindManyPrizeArgs } from "db"

type GetPrizesInput = {
  where?: FindManyPrizeArgs["where"]
  orderBy?: FindManyPrizeArgs["orderBy"]
  skip?: FindManyPrizeArgs["skip"]
  take?: FindManyPrizeArgs["take"]
  // Only available if a model relationship exists
  // include?: FindManyPrizeArgs['include']
}

export default async function getPrizes() {
  const allPrizes = await db.prize.findMany({
    where: {
      quantity: {
        gt: 0,
      },
    },
  })
  const firstPlace = allPrizes[0]

  return {
    firstPlace,
  }
}
