import React from "react"
import Layout from "app/layouts/Layout"
import { Head, Link, useRouter, BlitzPage } from "blitz"
import createPrize from "app/prizes/mutations/createPrize"
import PrizeForm from "app/prizes/components/PrizeForm"

const NewPrizePage: BlitzPage = () => {
  const router = useRouter()

  return (
    <div>
      <Head>
        <title>New Prize</title>
      </Head>

      <main>
        <h1>Create New Prize</h1>

        <PrizeForm
          initialValues={{}}
          onSubmit={async () => {
            try {
              const prize = await createPrize({ data: { name: "MyName" } })
              alert("Success!" + JSON.stringify(prize))
              router.push("/prizes/[prizeId]", `/prizes/${prize.id}`)
            } catch (error) {
              alert("Error creating prize " + JSON.stringify(error, null, 2))
            }
          }}
        />

        <p>
          <Link href="/prizes">
            <a>Prizes</a>
          </Link>
        </p>
      </main>
    </div>
  )
}

NewPrizePage.getLayout = (page) => <Layout title={"Create New Prize"}>{page}</Layout>

export default NewPrizePage
