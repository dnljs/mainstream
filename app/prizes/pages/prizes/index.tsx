import React, { Suspense } from "react"
import Layout from "app/layouts/Layout"
import { Head, Link, usePaginatedQuery, useRouter, BlitzPage } from "blitz"
import getPrizes from "app/prizes/queries/getPrizes"

const ITEMS_PER_PAGE = 100

export const PrizesList = () => {
  const router = useRouter()
  const page = Number(router.query.page) || 0
  const [{ prizes, hasMore }] = usePaginatedQuery(getPrizes, {
    orderBy: { id: "asc" },
    skip: ITEMS_PER_PAGE * page,
    take: ITEMS_PER_PAGE,
  })

  const goToPreviousPage = () => router.push({ query: { page: page - 1 } })
  const goToNextPage = () => router.push({ query: { page: page + 1 } })

  return (
    <div>
      <ul>
        {prizes.map((prize) => (
          <li key={prize.id}>
            <Link href="/prizes/[prizeId]" as={`/prizes/${prize.id}`}>
              <a>{prize.name}</a>
            </Link>
          </li>
        ))}
      </ul>

      <button disabled={page === 0} onClick={goToPreviousPage}>
        Previous
      </button>
      <button disabled={!hasMore} onClick={goToNextPage}>
        Next
      </button>
    </div>
  )
}

const PrizesPage: BlitzPage = () => {
  return (
    <div>
      <Head>
        <title>Prizes</title>
      </Head>

      <main>
        <h1>Prizes</h1>

        <p>
          <Link href="/prizes/new">
            <a>Create Prize</a>
          </Link>
        </p>

        <Suspense fallback={<div>Loading...</div>}>
          <PrizesList />
        </Suspense>
      </main>
    </div>
  )
}

PrizesPage.getLayout = (page) => <Layout title={"Prizes"}>{page}</Layout>

export default PrizesPage
