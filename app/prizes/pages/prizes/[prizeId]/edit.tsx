import React, { Suspense } from "react"
import Layout from "app/layouts/Layout"
import { Head, Link, useRouter, useQuery, useParam, BlitzPage } from "blitz"
import getPrize from "app/prizes/queries/getPrize"
import updatePrize from "app/prizes/mutations/updatePrize"
import PrizeForm from "app/prizes/components/PrizeForm"

export const EditPrize = () => {
  const router = useRouter()
  const prizeId = useParam("prizeId", "number")
  const [prize, { mutate }] = useQuery(getPrize, { where: { id: prizeId } })

  return (
    <div>
      <h1>Edit Prize {prize.id}</h1>
      <pre>{JSON.stringify(prize)}</pre>

      <PrizeForm
        initialValues={prize}
        onSubmit={async () => {
          try {
            const updated = await updatePrize({
              where: { id: prize.id },
              data: { name: "MyNewName" },
            })
            mutate(updated)
            alert("Success!" + JSON.stringify(updated))
            router.push("/prizes/[prizeId]", `/prizes/${updated.id}`)
          } catch (error) {
            console.log(error)
            alert("Error creating prize " + JSON.stringify(error, null, 2))
          }
        }}
      />
    </div>
  )
}

const EditPrizePage: BlitzPage = () => {
  return (
    <div>
      <Head>
        <title>Edit Prize</title>
      </Head>

      <main>
        <Suspense fallback={<div>Loading...</div>}>
          <EditPrize />
        </Suspense>

        <p>
          <Link href="/prizes">
            <a>Prizes</a>
          </Link>
        </p>
      </main>
    </div>
  )
}

EditPrizePage.getLayout = (page) => <Layout title={"Edit Prize"}>{page}</Layout>

export default EditPrizePage
