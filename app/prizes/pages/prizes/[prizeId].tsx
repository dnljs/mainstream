import React, { Suspense } from "react"
import Layout from "app/layouts/Layout"
import { Head, Link, useRouter, useQuery, useParam, BlitzPage } from "blitz"
import getPrize from "app/prizes/queries/getPrize"
import deletePrize from "app/prizes/mutations/deletePrize"

export const Prize = () => {
  const router = useRouter()
  const prizeId = useParam("prizeId", "number")
  const [prize] = useQuery(getPrize, { where: { id: prizeId } })

  return (
    <div>
      <h1>Prize {prize.id}</h1>
      <pre>{JSON.stringify(prize, null, 2)}</pre>

      <Link href="/prizes/[prizeId]/edit" as={`/prizes/${prize.id}/edit`}>
        <a>Edit</a>
      </Link>

      <button
        type="button"
        onClick={async () => {
          if (window.confirm("This will be deleted")) {
            await deletePrize({ where: { id: prize.id } })
            router.push("/prizes")
          }
        }}
      >
        Delete
      </button>
    </div>
  )
}

const ShowPrizePage: BlitzPage = () => {
  return (
    <div>
      <Head>
        <title>Prize</title>
      </Head>

      <main>
        <p>
          <Link href="/prizes">
            <a>Prizes</a>
          </Link>
        </p>

        <Suspense fallback={<div>Loading...</div>}>
          <Prize />
        </Suspense>
      </main>
    </div>
  )
}

ShowPrizePage.getLayout = (page) => <Layout title={"Prize"}>{page}</Layout>

export default ShowPrizePage
