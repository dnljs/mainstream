import React from "react"
import Layout from "app/layouts/Layout"
import { Head, Link, useRouter, BlitzPage } from "blitz"
import createLead from "app/leads/mutations/createLead"
import LeadForm from "app/leads/components/LeadForm"

const NewLeadPage: BlitzPage = () => {
  const router = useRouter()

  return (
    <div>
      <Head>
        <title>New Lead</title>
      </Head>

      <main>
        <h1>Create New Lead</h1>

        <LeadForm
          initialValues={{}}
          onSubmit={async () => {
            try {
              const lead = await createLead({ data: { name: "MyName" } })
              alert("Success!" + JSON.stringify(lead))
              router.push("/leads/[leadId]", `/leads/${lead.id}`)
            } catch (error) {
              alert("Error creating lead " + JSON.stringify(error, null, 2))
            }
          }}
        />

        <p>
          <Link href="/leads">
            <a>Leads</a>
          </Link>
        </p>
      </main>
    </div>
  )
}

NewLeadPage.getLayout = (page) => <Layout title={"Create New Lead"}>{page}</Layout>

export default NewLeadPage
