import React, { Suspense } from "react"
import Layout from "app/layouts/Layout"
import { Head, Link, useRouter, useQuery, useParam, BlitzPage } from "blitz"
import getLead from "app/leads/queries/getLead"
import updateLead from "app/leads/mutations/updateLead"
import LeadForm from "app/leads/components/LeadForm"

export const EditLead = () => {
  const router = useRouter()
  const leadId = useParam("leadId", "number")
  const [lead, { mutate }] = useQuery(getLead, { where: { id: leadId } })

  return (
    <div>
      <h1>Edit Lead {lead.id}</h1>
      <pre>{JSON.stringify(lead)}</pre>

      <LeadForm
        initialValues={lead}
        onSubmit={async () => {
          try {
            const updated = await updateLead({
              where: { id: lead.id },
              data: { name: "MyNewName" },
            })
            mutate(updated)
            alert("Success!" + JSON.stringify(updated))
            router.push("/leads/[leadId]", `/leads/${updated.id}`)
          } catch (error) {
            console.log(error)
            alert("Error creating lead " + JSON.stringify(error, null, 2))
          }
        }}
      />
    </div>
  )
}

const EditLeadPage: BlitzPage = () => {
  return (
    <div>
      <Head>
        <title>Edit Lead</title>
      </Head>

      <main>
        <Suspense fallback={<div>Loading...</div>}>
          <EditLead />
        </Suspense>

        <p>
          <Link href="/leads">
            <a>Leads</a>
          </Link>
        </p>
      </main>
    </div>
  )
}

EditLeadPage.getLayout = (page) => <Layout title={"Edit Lead"}>{page}</Layout>

export default EditLeadPage
