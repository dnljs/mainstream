import React, { Suspense } from "react"
import Layout from "app/layouts/Layout"
import { Head, Link, useRouter, useQuery, useParam, BlitzPage } from "blitz"
import getLead from "app/leads/queries/getLead"
import deleteLead from "app/leads/mutations/deleteLead"

export const Lead = () => {
  const router = useRouter()
  const leadId = useParam("leadId", "number")
  const [lead] = useQuery(getLead, { where: { id: leadId } })

  return (
    <div>
      <h1>Lead {lead.id}</h1>
      <pre>{JSON.stringify(lead, null, 2)}</pre>

      <Link href="/leads/[leadId]/edit" as={`/leads/${lead.id}/edit`}>
        <a>Edit</a>
      </Link>

      <button
        type="button"
        onClick={async () => {
          if (window.confirm("This will be deleted")) {
            await deleteLead({ where: { id: lead.id } })
            router.push("/leads")
          }
        }}
      >
        Delete
      </button>
    </div>
  )
}

const ShowLeadPage: BlitzPage = () => {
  return (
    <div>
      <Head>
        <title>Lead</title>
      </Head>

      <main>
        <p>
          <Link href="/leads">
            <a>Leads</a>
          </Link>
        </p>

        <Suspense fallback={<div>Loading...</div>}>
          <Lead />
        </Suspense>
      </main>
    </div>
  )
}

ShowLeadPage.getLayout = (page) => <Layout title={"Lead"}>{page}</Layout>

export default ShowLeadPage
