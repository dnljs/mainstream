import React, { Suspense } from "react"
import Layout from "app/layouts/Layout"
import { Head, Link, usePaginatedQuery, useRouter, BlitzPage } from "blitz"
import getLeads from "app/leads/queries/getLeads"

const ITEMS_PER_PAGE = 100

export const LeadsList = () => {
  const router = useRouter()
  const page = Number(router.query.page) || 0
  const [{ leads, hasMore }] = usePaginatedQuery(getLeads, {
    orderBy: { id: "asc" },
    skip: ITEMS_PER_PAGE * page,
    take: ITEMS_PER_PAGE,
  })

  const goToPreviousPage = () => router.push({ query: { page: page - 1 } })
  const goToNextPage = () => router.push({ query: { page: page + 1 } })

  return (
    <div>
      <ul>
        {leads.map((lead) => (
          <li key={lead.id}>
            <Link href="/leads/[leadId]" as={`/leads/${lead.id}`}>
              <a>{lead.name}</a>
            </Link>
          </li>
        ))}
      </ul>

      <button disabled={page === 0} onClick={goToPreviousPage}>
        Previous
      </button>
      <button disabled={!hasMore} onClick={goToNextPage}>
        Next
      </button>
    </div>
  )
}

const LeadsPage: BlitzPage = () => {
  return (
    <div>
      <Head>
        <title>Leads</title>
      </Head>

      <main>
        <h1>Leads</h1>

        <p>
          <Link href="/leads/new">
            <a>Create Lead</a>
          </Link>
        </p>

        <Suspense fallback={<div>Loading...</div>}>
          <LeadsList />
        </Suspense>
      </main>
    </div>
  )
}

LeadsPage.getLayout = (page) => <Layout title={"Leads"}>{page}</Layout>

export default LeadsPage
