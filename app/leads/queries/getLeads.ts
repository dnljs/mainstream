import { SessionContext } from "blitz"
import db  from "db"

export default async function getLeads() {
  const leads = await db.lead.findMany({})
  return {
    leads,
  }
}
