import db, { FindOneLeadArgs } from "db"

type GetLeadInput = {
  where: FindOneLeadArgs["where"]
  // Only available if a model relationship exists
  // include?: FindOneLeadArgs['include']
}

export default async function isExists({ where /* include */ }: GetLeadInput) {
  const lead = await db.lead.findOne({ where })
  if (!lead) {
    return true
  }
  if (lead) {
    return false
  }
}
