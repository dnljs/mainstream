import { SessionContext } from "blitz"
import db, { FindOneLeadArgs } from "db";

type getLeadInputs = {
  where: FindOneLeadArgs["where"]
  // Only available if a model relationship exists
  // include?: FindOnePrizeArgs['include']
}

export default async function getLead({ where  /* include */ }: getLeadInputs , { data }: any) {
  const lead = await db.lead.findOne({ where })
  return !lead
}
