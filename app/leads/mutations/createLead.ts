import { SessionContext } from "blitz"
import db, { LeadCreateArgs } from "db"

type CreateLeadInput = {
  data: LeadCreateArgs["data"]
}
export default async function createLead({ data }: CreateLeadInput) {
  const lead = await db.lead.create({ data })
  return lead
}
