import { SessionContext } from "blitz"
import db, { LeadUpdateArgs } from "db"

type UpdateLeadInput = {
  where: LeadUpdateArgs["where"]
  data: LeadUpdateArgs["data"]
}

export default async function updateLead(
  { where, data }: UpdateLeadInput,
  ctx: { session?: SessionContext } = {}
) {
  ctx.session!.authorize()

  const lead = await db.lead.update({ where, data })

  return lead
}
