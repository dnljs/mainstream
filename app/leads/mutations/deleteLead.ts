import { SessionContext } from "blitz"
import db, { LeadDeleteArgs } from "db"

type DeleteLeadInput = {
  where: LeadDeleteArgs["where"]
}

export default async function deleteLead(
  { where }: DeleteLeadInput,
  ctx: { session?: SessionContext } = {}
) {
  ctx.session!.authorize()

  const lead = await db.lead.delete({ where })

  return lead
}
